package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.bpmn2.modeler.core.utils.ModelUtil.Bpmn2DiagramType;
import org.eclipse.bpmn2.modeler.ui.AbstractBpmn2RuntimeExtension;
import org.eclipse.bpmn2.modeler.ui.wizards.FileService;
import org.eclipse.ui.IEditorInput;
import org.xml.sax.InputSource;

public class ChorevolutionBPMN2ChoreographyRuntimeExtension extends AbstractBpmn2RuntimeExtension {

	public static final String RUNTIME_ID = "eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.runtime";

	public static String targetNamespace = "http://chorevolution.org/bpmn2";

	@Override
	public String getTargetNamespace(Bpmn2DiagramType diagramType) {
		return targetNamespace;
	}

	@Override
	public boolean isContentForRuntime(IEditorInput input) {
		// allow targetnamespace user declaration
		try {
			InputSource source = new InputSource(FileService.getInputContents(input));

			XPathFactory xpf = XPathFactory.newInstance();
			XPath xpath = xpf.newXPath();

			String res = (String) xpath.evaluate("//@targetNamespace", source, XPathConstants.STRING);
			targetNamespace = res.isEmpty() ? targetNamespace : res;

			RootElementParser parser = new RootElementParser(targetNamespace);
			parser.parse(source);
			return true;
		} catch (XPathExpressionException e) {
			return true;
		}
	}
}
