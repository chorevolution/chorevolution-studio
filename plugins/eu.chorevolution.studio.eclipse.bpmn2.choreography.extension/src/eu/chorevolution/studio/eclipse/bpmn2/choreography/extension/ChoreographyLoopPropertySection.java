/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.ChoreographyActivity;
import org.eclipse.bpmn2.ChoreographyLoopType;
import org.eclipse.bpmn2.Message;
import org.eclipse.bpmn2.modeler.core.merrimac.clad.AbstractBpmn2PropertySection;
import org.eclipse.bpmn2.modeler.core.merrimac.clad.AbstractDetailComposite;
import org.eclipse.bpmn2.modeler.core.merrimac.dialogs.TextObjectEditor;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.Value;
import eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.ui.ChorevolutionBPMN2ChoreographyDetailComposite;

/**
 * This PorpertySection provides the attributes for Loop config.
 * 
 *
 */
public class ChoreographyLoopPropertySection extends AbstractPropertySection {
	public static final String XSD_FILE_NAME = "types";
	public static final String XSD_FILE_EXTENSION = "xsd";

	private TextObjectEditor textObjectConditionalExpression, textObjectNumericExpression;
	private Button btnCheckConditionalExpression, btnCheckNumericExpression;

	private Button openNumericExpression;
	private Button openConditionalExpression;

	private ChoreographyActivity selectedChor;

	@Override
	protected AbstractDetailComposite createSectionRoot() {
		return new LoopProperties(this);
	}

	@Override
	public AbstractDetailComposite createSectionRoot(Composite parent, int style) {
		return new LoopProperties(parent, style);
	}

	public class LoopProperties extends ChorevolutionBPMN2ChoreographyDetailComposite {
		//private ChoreographyLoopType loopType;

		@Override
		public void dispose() {
			selectedChor.eAdapters().remove(selectedChor.eAdapters().size() - 1);
			super.dispose();
		}

		public LoopProperties(AbstractBpmn2PropertySection section) {
			super(section);
		}

		public LoopProperties(Composite parent, int style) {
			super(parent, style);
		}

		@Override
		public void createBindings(final EObject be) {
			setTitle("Loop Condition");

			ChoreographyActivity ca = (ChoreographyActivity) be;
			selectedChor = ca;

			Composite loopComposite = toolkit.createComposite(attributesComposite);
			GridData data = new GridData(SWT.FILL, SWT.TOP, true, true);
			loopComposite.setLayout(new GridLayout(4, false));
			data.horizontalSpan = 2;
			loopComposite.setLayoutData(data);

			Label label = new Label(loopComposite, SWT.CENTER);
			label.setText("Expression Type:");
			new Label(loopComposite, SWT.CENTER);

			Group group = new Group(loopComposite, SWT.SHADOW_NONE);
			group.setLayout(new RowLayout(SWT.HORIZONTAL));

			SelectionListener numericSelectionListener = new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if (!((Button) e.getSource()).getSelection()) {
						return;
					}

					if (btnCheckNumericExpression.getEnabled() == true) {
						openNumericExpression.setEnabled(true);
						openConditionalExpression.setEnabled(false);
					}
				}
			};

			SelectionListener conditionalSelectionListener = new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if (!((Button) e.getSource()).getSelection()) {
						return;
					}

					if (btnCheckConditionalExpression.getEnabled() == true) {
						openConditionalExpression.setEnabled(true);
						openNumericExpression.setEnabled(false);
					}
				}
			};

			btnCheckConditionalExpression = new Button(group, SWT.RADIO);
			btnCheckConditionalExpression.setBounds(32, 35, 93, 16);
			btnCheckConditionalExpression.setText("Conditional");
			btnCheckConditionalExpression.addSelectionListener(conditionalSelectionListener);

			btnCheckNumericExpression = new Button(group, SWT.RADIO);
			btnCheckNumericExpression.setBounds(32, 35, 93, 16);
			btnCheckNumericExpression.setText("Numeric");
			btnCheckNumericExpression.addSelectionListener(numericSelectionListener);

			new Label(loopComposite, SWT.CENTER);

			// Data ref
			Value value = ChorevoutionBPMN2ChoreographyExtensionPlugin.getItemValueByName((BaseElement) be,
					"loopConditionalExpression", "CDATA", "");
			textObjectConditionalExpression = new TextObjectEditor(this, value,
					ChorevoutionBPMN2ChoreographyExtensionPlugin._ITEMVALUE);
			// textObjectConditionalExpression.setMultiLine(true);
			textObjectConditionalExpression.createControl(loopComposite, "Conditional Expression:");
			textObjectConditionalExpression.setEditable(false);

			openConditionalExpression = new Button(loopComposite, SWT.CENTER | SWT.BEGINNING);
			openConditionalExpression.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false));
			openConditionalExpression.setText("Build Expression");
			openConditionalExpression.addMouseListener(new MouseListener() {

				@Override
				public void mouseDoubleClick(MouseEvent e) {

				}

				@Override
				public void mouseDown(MouseEvent e) {
					if (btnCheckConditionalExpression.getSelection()) {
						launchExpressionBuilder(be, "Conditional");
					}
				}

				@Override
				public void mouseUp(MouseEvent e) {
				}
			});

			value = ChorevoutionBPMN2ChoreographyExtensionPlugin.getItemValueByName((BaseElement) be,
					"loopNumericExpression", "CDATA", "");
			textObjectNumericExpression = new TextObjectEditor(this, value,
					ChorevoutionBPMN2ChoreographyExtensionPlugin._ITEMVALUE);
			// textObjectNumericExpression.setMultiLine(true);
			textObjectNumericExpression.createControl(loopComposite, "Numeric Expression:");
			textObjectNumericExpression.setEditable(false);

			openNumericExpression = new Button(loopComposite, SWT.CENTER | SWT.BEGINNING);
			openNumericExpression.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false));
			openNumericExpression.setText("Build Expression");
			openNumericExpression.addMouseListener(new MouseListener() {

				@Override
				public void mouseDoubleClick(MouseEvent e) {
				}

				@Override
				public void mouseDown(MouseEvent e) {
					if (btnCheckNumericExpression.getSelection()) {
						launchExpressionBuilder(be, "Numeric");
					}
				}

				@Override
				public void mouseUp(MouseEvent e) {
				}
			});

			// initialize objects
			openConditionalExpression.setEnabled(false);
			openNumericExpression.setEnabled(false);
			if (ca.getLoopType() == ChoreographyLoopType.NONE) {
				textObjectConditionalExpression.setValue("");
				textObjectNumericExpression.setValue("");
				btnCheckNumericExpression.setSelection(false);
				btnCheckConditionalExpression.setSelection(false);
				btnCheckConditionalExpression.setEnabled(false);
				btnCheckNumericExpression.setEnabled(false);
				openConditionalExpression.setEnabled(false);
				openNumericExpression.setEnabled(false);
			} else if (ca.getLoopType() == ChoreographyLoopType.MULTI_INSTANCE_PARALLEL
					|| ca.getLoopType() == ChoreographyLoopType.MULTI_INSTANCE_SEQUENTIAL) {
				btnCheckNumericExpression.setSelection(true);
				openNumericExpression.setEnabled(true);
				btnCheckConditionalExpression.setEnabled(false);
				btnCheckNumericExpression.setEnabled(true);
				openConditionalExpression.setEnabled(false);
				textObjectConditionalExpression.setValue("");
			}
			if (textObjectNumericExpression.getValue().equals("")
					&& !textObjectConditionalExpression.getValue().equals("")) {
				btnCheckConditionalExpression.setSelection(true);
				openNumericExpression.setEnabled(false);
				openConditionalExpression.setEnabled(true);
			} else if (!textObjectNumericExpression.getValue().equals("")
					&& textObjectConditionalExpression.getValue().equals("")) {
				btnCheckNumericExpression.setSelection(true);
				openConditionalExpression.setEnabled(false);
				openNumericExpression.setEnabled(true);
			}

			ca.eAdapters().add(new AdapterImpl() {
				public void notifyChanged(Notification notification) {
					int type = notification.getEventType();

					if (type == Notification.SET) {
						Object newValue = notification.getNewValue();
						if (newValue != null) {
							// enable selection according to loop type
							if (ca.getLoopType() == ChoreographyLoopType.NONE) {
								textObjectConditionalExpression.setValue("");
								textObjectNumericExpression.setValue("");
								btnCheckConditionalExpression.setSelection(false);
								btnCheckNumericExpression.setSelection(false);
								btnCheckNumericExpression.setEnabled(false);
								btnCheckConditionalExpression.setEnabled(false);
								openConditionalExpression.setEnabled(false);
								openNumericExpression.setEnabled(false);
							}
							if (ca.getLoopType() == ChoreographyLoopType.STANDARD) {
								btnCheckConditionalExpression.setEnabled(true);
								btnCheckNumericExpression.setEnabled(true);
								btnCheckConditionalExpression.setSelection(true);
								btnCheckNumericExpression.setSelection(false);

								btnCheckConditionalExpression.notifyListeners(SWT.Selection, new Event());
							}
							if (ca.getLoopType() == ChoreographyLoopType.MULTI_INSTANCE_PARALLEL) {
								btnCheckNumericExpression.setSelection(true);
								btnCheckConditionalExpression.setSelection(false);
								btnCheckNumericExpression.setEnabled(true);
								openNumericExpression.setEnabled(true);
								btnCheckConditionalExpression.setEnabled(false);
								openConditionalExpression.setEnabled(false);
								textObjectConditionalExpression.setValue("");

								btnCheckNumericExpression.notifyListeners(SWT.Selection, new Event());
							}
							if (ca.getLoopType() == ChoreographyLoopType.MULTI_INSTANCE_SEQUENTIAL) {
								btnCheckNumericExpression.setSelection(true);
								btnCheckConditionalExpression.setSelection(false);
								btnCheckNumericExpression.setEnabled(true);
								openNumericExpression.setEnabled(true);
								btnCheckConditionalExpression.setEnabled(false);
								openConditionalExpression.setEnabled(false);
								textObjectConditionalExpression.setValue("");

								btnCheckNumericExpression.notifyListeners(SWT.Selection, new Event());
							}

						}
					}
				}
			});
		}
	}

	private List<ChorevolutionBPMN2ChoreographyExpressionType> parseTypes(File typesFile,
			List<ChorevolutionBPMN2ChoreographyExpressionType> bpmn2ChoreographyMessages) {
		List<ChorevolutionBPMN2ChoreographyExpressionType> explodedTypeList = new ArrayList<ChorevolutionBPMN2ChoreographyExpressionType>();
		try {

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new FileInputStream(typesFile));

			XPath xPath = XPathFactory.newInstance().newXPath();

			for (ChorevolutionBPMN2ChoreographyExpressionType t : bpmn2ChoreographyMessages) {
				java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> etl = getTypes(doc, xPath, t.getType(),
						t.getName());
				explodedTypeList.addAll(etl);
			}

		} catch (ParserConfigurationException | SAXException | IOException e1) {
			MessageDialog.openError(ChorevoutionBPMN2ChoreographyExtensionPlugin.getActiveWorkbenchShell(),
					ChorevoutionBPMN2ChoreographyExtensionMessages.BPMN2MessageFile_Error,
					NLS.bind(ChorevoutionBPMN2ChoreographyExtensionMessages.BPMN2MessageFile_ParsingError,
							e1.getMessage()));
			e1.printStackTrace();
		}

		return explodedTypeList;
	}

	private void launchExpressionBuilder(EObject be, String tipology) {
		TreeIterator<EObject> it = be.eResource().getAllContents();

		java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> typeList = new ArrayList<>();
		while (it.hasNext()) {
			EObject o = it.next();
			if (o instanceof Message) {
				Message m = (Message) o;
				ChorevolutionBPMN2ChoreographyExpressionType type = new ChorevolutionBPMN2ChoreographyExpressionType();
				type.setName(m.getName());
				type.setType(m.getItemRef().getStructureRef().toString().split(":")[1]);
				typeList.add(type);
			}
		}

		// NOTE this is a workaround, the main function is bugged
		// and not returns the relative project.
		IPath relativeUrl = new Path(ChorevoutionBPMN2ChoreographyExtensionPlugin.getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor().getEditorInput().getToolTipText()).removeLastSegments(1)
						.append(XSD_FILE_NAME).addFileExtension(XSD_FILE_EXTENSION);
		File typesFile = ChorevoutionBPMN2ChoreographyExtensionPlugin.getWorkspace().getRoot().getFile(relativeUrl)
				.getRawLocation().makeAbsolute().toFile();
		if (!typesFile.exists()) {
			MessageDialog.openError(ChorevoutionBPMN2ChoreographyExtensionPlugin.getActiveWorkbenchShell(),
					ChorevoutionBPMN2ChoreographyExtensionMessages.BPMN2MessageFile_Error,
					NLS.bind(ChorevoutionBPMN2ChoreographyExtensionMessages.BPMN2MessageFile_notFound,
							typesFile.getName(), typesFile.getAbsolutePath()));
		} else {
			// ChorevolutionBPMN2ChoreographyExpressionBuilder
			// dialog = new
			// ChorevolutionBPMN2ChoreographyExpressionBuilder(getShell(),
			// parseTypes(typesFile, typeList));
			int expressionBuilderType;
			String currentExpression;
			if (tipology.equals("Conditional")) {
				expressionBuilderType = ChorevolutionBPMN2ChoreographyExpressionBuilder.IS_CONDITIONAL;
				currentExpression = textObjectConditionalExpression.getValue().toString();
			} else {
				expressionBuilderType = ChorevolutionBPMN2ChoreographyExpressionBuilder.IS_NUMERIC;
				currentExpression = textObjectNumericExpression.getValue().toString();
			}

			ChorevolutionBPMN2ChoreographyExpressionBuilder dialog = new ChorevolutionBPMN2ChoreographyExpressionBuilder(
					ChorevoutionBPMN2ChoreographyExtensionPlugin.getActiveWorkbenchShell(), tipology,
					expressionBuilderType, currentExpression, parseTypes(typesFile, typeList));
			// get the new values from the dialog
			if (dialog.open() == Window.OK) {
				if (dialog.isValid_expression()) {
					// field reset
					textObjectConditionalExpression.setValue("");
					textObjectNumericExpression.setValue("");

					String text_expression = dialog.getExpression();
					int _type_exp = dialog.getType_expression();
					if (_type_exp == ChorevolutionBPMN2ChoreographyExpressionBuilder.IS_NUMERIC) {
						if (btnCheckNumericExpression.getSelection()) {
							textObjectNumericExpression.setValue(text_expression);
						}
					} else if (_type_exp == ChorevolutionBPMN2ChoreographyExpressionBuilder.IS_CONDITIONAL) {
						if (btnCheckConditionalExpression.getSelection()) {
							textObjectConditionalExpression.setValue(text_expression);
						}
					}
				}
			}
		}
	}

	// build a list of variables and simple types from given complex type list
	private java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> getTypes(Document doc, XPath xpath,
			String typename, String prefix) {
		java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> list = new java.util.ArrayList<>();
		try {
			// create XPathExpression object
			XPathExpression expr = xpath.compile("/schema/complexType[@name='" + typename + "']/sequence/element");
			// evaluate expression result on XML document
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				String type[] = nodes.item(i).getAttributes().getNamedItem("type").getNodeValue().split(":");
				String name = nodes.item(i).getAttributes().getNamedItem("name").getNodeValue();
				Node occurs = nodes.item(i).getParentNode().getAttributes().getNamedItem("minOccurs");
				if (type[0].equals("tns")) {
					if (occurs != null) {
						ChorevolutionBPMN2ChoreographyExpressionType t = new ChorevolutionBPMN2ChoreographyExpressionType();
						t.setName("/" + prefix + "/" + name);
						name += "[]";
						t.setType(type[1]);
						list.add(t);
					}
					list.addAll(getTypes(doc, xpath, type[1], prefix + "/" + name));
				} else {
					ChorevolutionBPMN2ChoreographyExpressionType t = new ChorevolutionBPMN2ChoreographyExpressionType();
					t.setName("/" + prefix + "/" + name);
					t.setType(type[1]);
					list.add(t);
				}
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list;
	}

}