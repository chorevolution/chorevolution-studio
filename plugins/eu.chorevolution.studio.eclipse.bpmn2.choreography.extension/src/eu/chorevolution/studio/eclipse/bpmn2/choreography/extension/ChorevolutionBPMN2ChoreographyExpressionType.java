package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension;

public class ChorevolutionBPMN2ChoreographyExpressionType {
	String name;
	String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
