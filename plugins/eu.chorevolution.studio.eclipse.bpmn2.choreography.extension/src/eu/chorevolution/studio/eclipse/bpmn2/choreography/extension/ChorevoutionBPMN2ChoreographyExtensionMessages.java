/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension;

import org.eclipse.osgi.util.NLS;

public final class ChorevoutionBPMN2ChoreographyExtensionMessages extends NLS {

	private static final String BUNDLE_NAME = ChorevoutionBPMN2ChoreographyExtensionPlugin.PLUGIN_ID + ".ChorevoutionBPMN2ChoreographyExtensionMessages";

	private ChorevoutionBPMN2ChoreographyExtensionMessages() {
		// Do not instantiate
	}

	public static String BPMN2MessageFile_Error;
	public static String BPMN2MessageFile_ParsingError;
	public static String BPMN2MessageFile_notFound;
	
	static {
		NLS.initializeMessages(BUNDLE_NAME, ChorevoutionBPMN2ChoreographyExtensionMessages.class);
	}
}
