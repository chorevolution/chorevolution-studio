/**
 */
package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.Value#getMixed <em>Mixed</em>}</li>
 *   <li>{@link eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.Value#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.ModelPackage#getValue()
 * @model extendedMetaData="kind='mixed' namespace='##targetNamespace'"
 * @generated
 */
public interface Value extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mixed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.ModelPackage#getValue_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed' namespace='##targetNamespace'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.ModelPackage#getValue_Value()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.Value#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // Value
