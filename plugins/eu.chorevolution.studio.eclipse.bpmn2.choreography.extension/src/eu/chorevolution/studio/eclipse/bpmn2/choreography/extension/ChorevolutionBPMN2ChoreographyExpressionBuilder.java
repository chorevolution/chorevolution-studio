package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlException;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.MapContext;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ChorevolutionBPMN2ChoreographyExpressionBuilder extends Dialog {
	public static final int IS_INVALID = -1;
	public static final int IS_NUMERIC = 0;
	public static final int IS_CONDITIONAL = 1;

	private String text_expression = "";
	private String validationResultValue = "";
	private String basicExpression = "";
	private boolean valid_expression = false;
	private int type_expression;
	private int expressionBuilderType;

	private String sel_var_list = "";

	private String titleShell;
	private java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> explodedTypeList;
	ArrayList<String> functions = new ArrayList<String>(Arrays.asList("function list", "not", "true", "false", "number",
			"count", "sum", "floor", "ceiling", "round"));
	ArrayList<String> operands = new ArrayList<String>(
			Arrays.asList("div", "*", "-", "+", ">", "!", "=", "<", "(", ")", "[", "]", "and", "or", "not"));

	private Document doc;

	//////////////////////////////////
	private Text textExpression;
	private Group groupButtons;
	private Button button_add;
	private Button button_subtr;
	private Button button_mult;
	private Button button_div;
	private Button button_equals;
	private Button button_minor;
	private Button button_minor_eq;
	private Button button_different;
	private Button button_major_eq;
	private Button button_major;
	private Button button_and;
	private Button button_or;
	private Button button_not;
	private Button button_right_par;
	private Button button_left_par;
	private Button btnClear;
	private Group groupRight;
	private List listVariables;
	private Combo listFunctions;
	private Group groupVariables;
	private Button buttonRemove;
	private Button buttonAdd;
	private Label validationResult;
	private String validationTitle;
	private String currentExpression;

	public ChorevolutionBPMN2ChoreographyExpressionBuilder(Shell parent, String titleShell, int expressionBuilderType,
			String currentExpression, java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> explodedTypeList) {
		super(parent);
		this.expressionBuilderType = expressionBuilderType;
		this.validationTitle = titleShell;
		this.titleShell = "Build " + titleShell + " Expression";
		this.basicExpression = "Validation " + titleShell + " Expression: ";
		this.currentExpression = currentExpression;
		this.explodedTypeList = explodedTypeList;

		// NOTE this is a workaround, the main function is bugged
		// and not returns the relative project.
		IPath relativeUrl = new Path(ChorevoutionBPMN2ChoreographyExtensionPlugin.getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor().getEditorInput().getToolTipText()).removeLastSegments(1)
						.append(ChoreographyLoopPropertySection.XSD_FILE_NAME)
						.addFileExtension(ChoreographyLoopPropertySection.XSD_FILE_EXTENSION);
		File typesFile = ChorevoutionBPMN2ChoreographyExtensionPlugin.getWorkspace().getRoot().getFile(relativeUrl)
				.getRawLocation().makeAbsolute().toFile();
		if (!typesFile.exists()) {
			MessageDialog.openError(ChorevoutionBPMN2ChoreographyExtensionPlugin.getActiveWorkbenchShell(),
					ChorevoutionBPMN2ChoreographyExtensionMessages.BPMN2MessageFile_Error,
					NLS.bind(ChorevoutionBPMN2ChoreographyExtensionMessages.BPMN2MessageFile_notFound,
							typesFile.getName(), typesFile.getAbsolutePath()));
		} else {

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			try {
				DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

				this.doc = docBuilder.parse(new FileInputStream(typesFile));
			} catch (ParserConfigurationException | SAXException | IOException e) {
				System.out.println(e.getLocalizedMessage());
			}
		}
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(titleShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		/*
		 * Composite container = new Composite(parent, SWT.NONE);
		 * //container.setLayout(new GridLayout(2, false));
		 * 
		 * GridLayout layout = new GridLayout(); layout.marginHeight = 3;
		 * layout.marginWidth = 3; layout.numColumns = 2;
		 * container.setLayout(layout); container.setLayoutData(new
		 * GridData(GridData.FILL_BOTH));
		 */

		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 3;
		layout.marginWidth = 3;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite container = new Composite(composite, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		GridLayout layoutFilterServices = new GridLayout();
		layoutFilterServices.numColumns = 2;
		layoutFilterServices.horizontalSpacing = 8;

		container.setLayout(layoutFilterServices);

		///////////////////////////////////////
		Group grpExpression = new Group(container, SWT.NONE);
		GridData fd_grpExpression = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1);
		grpExpression.setLayoutData(fd_grpExpression);
		grpExpression.setText("Expression");
		GridLayout layoutLeft = new GridLayout();
		layoutLeft.numColumns = 1;
		layoutLeft.horizontalSpacing = 8;
		grpExpression.setLayout(layoutLeft);
		// grpExpression.setLayout(new GridLayout(1, false));
		///////////////////////////////////////

		btnClear = new Button(grpExpression, SWT.NONE);
		btnClear.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnClear.setText("Clear");
		btnClear.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				textExpression.setText("");
			}
		});

		textExpression = new Text(grpExpression, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_text = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_text.widthHint = 498;
		gd_text.heightHint = 129;
		textExpression.setLayoutData(gd_text);
		textExpression.setText(currentExpression);
		textExpression.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent arg0) {
				evaluateFormula(textExpression.getText());
				getButton(IDialogConstants.OK_ID).setEnabled(valid_expression);
				validationResult.setText(validationResultValue);
			}
		});

		Label validationText = new Label(grpExpression, SWT.LEFT);
		validationText.setText(basicExpression);

		validationResult = new Label(grpExpression, SWT.LEFT);
		GridData validationLayout = new GridData(SWT.HORIZONTAL, SWT.TOP, true, true, 1, 1);
		validationResult.setLayoutData(validationLayout);
		validationResult.setText(
				"Please input some expression to validate. Be sure to write a " + validationTitle + " expression.");

		groupButtons = new Group(grpExpression, SWT.NONE);
		groupButtons.setLayout(new GridLayout(19, false));

		button_add = new Button(groupButtons, SWT.NONE);
		button_add.setText("+");
		button_add.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("+");
			}
		});

		button_subtr = new Button(groupButtons, SWT.NONE);
		button_subtr.setText("-");
		button_subtr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("-");
			}
		});

		button_mult = new Button(groupButtons, SWT.NONE);
		button_mult.setText("*");
		button_mult.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("*");
			}
		});

		button_div = new Button(groupButtons, SWT.NONE);
		button_div.setText("div");
		button_div.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("div");
			}
		});

		new Label(groupButtons, SWT.FILL);

		button_equals = new Button(groupButtons, SWT.NONE);
		button_equals.setText("=");
		button_equals.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("=");
			}
		});

		button_minor = new Button(groupButtons, SWT.NONE);
		button_minor.setText("<");
		button_minor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("<");
			}
		});

		button_minor_eq = new Button(groupButtons, SWT.NONE);
		button_minor_eq.setText("<=");
		button_minor_eq.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("<=");
			}
		});

		button_different = new Button(groupButtons, SWT.NONE);
		button_different.setText("!=");
		button_different.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("!=");
			}
		});

		button_major_eq = new Button(groupButtons, SWT.NONE);
		button_major_eq.setText(">=");
		button_major_eq.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText(">=");
			}
		});

		button_major = new Button(groupButtons, SWT.NONE);
		button_major.setText(">");
		button_major.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText(">");
			}
		});

		new Label(groupButtons, SWT.FILL);

		button_and = new Button(groupButtons, SWT.NONE);
		button_and.setText("and");
		button_and.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("and");
			}
		});

		button_or = new Button(groupButtons, SWT.NONE);
		button_or.setText("or");
		button_or.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("or");
			}
		});

		button_not = new Button(groupButtons, SWT.NONE);
		button_not.setText("not");
		button_not.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("not");
			}
		});

		new Label(groupButtons, SWT.FILL);

		button_left_par = new Button(groupButtons, SWT.NONE);
		button_left_par.setText("(");
		button_left_par.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText("(");
			}
		});

		button_right_par = new Button(groupButtons, SWT.NONE);
		button_right_par.setText(")");
		button_right_par.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText(")");
			}
		});

		listFunctions = new Combo(groupButtons, SWT.READ_ONLY);
		listFunctions.setBounds(50, 50, 150, 65);
		listFunctions.setItems(functions.toArray(new String[0]));
		listFunctions.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (!listFunctions.getText().equals("function list")) {
					addExpressionText(listFunctions.getText() + "()");
					listFunctions.select(0);
				}
			}
		});
		listFunctions.select(0);

		//// right side ////
		///////////////////////////////////////
		groupRight = new Group(container, SWT.NONE);
		// groupRight.setLayout(new GridLayout(1, false));
		groupRight.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 1, 1));
		GridLayout layoutRight = new GridLayout();
		layoutRight.numColumns = 1;
		layoutRight.horizontalSpacing = 8;
		groupRight.setLayout(layoutRight);
		groupRight.setText("Variables");
		///////////////////////////////////////

		listVariables = new List(groupRight, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gd_list_variables = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_list_variables.widthHint = 188;
		gd_list_variables.heightHint = 158;
		listVariables.setLayoutData(gd_list_variables);

		for (ChorevolutionBPMN2ChoreographyExpressionType tt : explodedTypeList) {
			listVariables.add(tt.getName());
		}

		listVariables.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent event) {
				String selections[] = listVariables.getSelection();
				String selection = "";
				for (int loopIndex = 0; loopIndex < selections.length; loopIndex++) {
					selection += selections[loopIndex] + " ";
				}
				sel_var_list = selection.trim();
			}

			public void widgetDefaultSelected(SelectionEvent event) {
				String selections[] = listVariables.getSelection();
				String selection = "";
				for (int loopIndex = 0; loopIndex < selections.length; loopIndex++) {
					selection += selections[loopIndex] + " ";
				}
				sel_var_list = selection.trim();
			}
		});
		listVariables.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {

			}

			@Override
			public void mouseDown(MouseEvent e) {

			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (e.count == 2) {
					addExpressionText(sel_var_list);
					evaluate(textExpression.getText());
				}
			}
		});

		groupVariables = new Group(groupRight, SWT.NONE);
		groupVariables.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		groupVariables.setLayout(new GridLayout(2, false));

		buttonAdd = new Button(groupVariables, SWT.NONE);
		buttonAdd.setText("Add");
		buttonAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addExpressionText(sel_var_list);
				evaluate(textExpression.getText());
			}
		});

		buttonRemove = new Button(groupVariables, SWT.NONE);
		buttonRemove.setText("Remove");
		buttonRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String exp = textExpression.getText();
				textExpression.setText(replaceLast(exp, sel_var_list, "").trim());
				textExpression.setSelection(textExpression.getText().length());
			}
		});

		//////////////////////////////

		return container;
	}

	private String replaceLast(String string, String toReplace, String replacement) {
		int pos = string.lastIndexOf(toReplace);
		if (pos > -1) {
			return string.substring(0, pos) + replacement + string.substring(pos + toReplace.length(), string.length());
		} else {
			return string;
		}
	}

	// jexl formula evaluator
	private Object evaluateJexl(String formula) throws JexlException, StringIndexOutOfBoundsException {
		JexlEngine jexl = new JexlBuilder().cache(512).strict(true).silent(false).create();

		JexlExpression expr = jexl.createExpression(formula);
		return expr.evaluate(new MapContext());
	}

	// xpath formula evaluator
	private String evaluateXPath(String formula) throws XPathExpressionException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = xPath.compile(formula);

		return (String) expr.evaluate(doc);
	}

	private void evaluateFormula(String formula) {
		try {
			String evaluate_formula = evaluate(formula);
			String evaluate_xpath = evaluateXPath(evaluate_formula);
			Object result = evaluateJexl(evaluate_xpath);

			if (result == null) {
				this.valid_expression = false;
				this.type_expression = IS_INVALID;
				this.validationResultValue = "The expression is invalid.";
			} else {
				// valid expression
				this.valid_expression = true;
				this.validationResultValue = " The expression is valid.";

				// conditional expression
				if (result instanceof Boolean) {
					this.type_expression = IS_CONDITIONAL;
					if (this.type_expression != expressionBuilderType) {
						this.valid_expression = false;
						this.type_expression = IS_INVALID;
						this.validationResultValue = "The expression is invalid. Please input a Conditional Expression.";
					}
				}
				// numeric expression
				if (result instanceof Number) {
					this.type_expression = IS_NUMERIC;
					if (this.type_expression != expressionBuilderType) {
						this.valid_expression = false;
						this.type_expression = IS_INVALID;
						this.validationResultValue = "The expression is invalid. Please input a Numeric Expression.";
					}
				}
			}
		} catch (StringIndexOutOfBoundsException | XPathExpressionException e) {
			System.out.println("Invalid expression: " + formula);
			this.valid_expression = false;
			this.type_expression = IS_INVALID;
			this.validationResultValue = "Please input some expression to validate. Be sure to write a "
					+ validationTitle + " expression.";
		} catch (JexlException e) {
			System.out.println("Invalid expression: " + formula);
			this.valid_expression = false;
			this.type_expression = IS_INVALID;
			this.validationResultValue = "The expression is syntactically invalid: " + formula;
		} catch (Exception e) {
			System.out.println("Invalid expression: " + formula);
			this.valid_expression = false;
			this.type_expression = IS_INVALID;
			this.validationResultValue = "The expression is syntactically invalid: " + formula;
		}
	}

	private String evaluate(String formula) {
		Random random = new Random();

		String evaluate_formula = "";
		String elements[] = formula.split("(?<=[*\\-+!>=<\\(\\)\\[\\s])|(?=[*\\-+!>=<\\(\\)\\]\\s])");
		// System.out.println(Arrays.toString(elements));
		for (int i = 0; i < elements.length; i++) {
			String element = elements[i];
			boolean operand_pass = false, function_pass = false, variable_pass = false, constant_pass = false;
			if (!(element.trim().isEmpty() || element.trim().startsWith("]"))) {
				// *** functions ***
				if (functions.contains(element.trim())) {
					evaluate_formula += element;
					function_pass = true;
				}
				// *** operands ***
				if (operands.contains(element.trim())) {
					evaluate_formula += element;
					operand_pass = true;
				}

				// *** variables ***
				java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> result = explodedTypeList.stream().filter(
						a -> (Objects.equals(a.name, element.trim()) || Objects.equals(a.name, element.trim() + "[]")))
						.collect(Collectors.toList());

				if (result.size() == 0) {
					if (element.trim().endsWith("[") && !element.trim().startsWith("]")) {
						
						String square_element = element.trim();
						int cnt_square_brackets = 1;
						for (int j = i + 1; j < elements.length; j++) {

							if (elements[j].trim().startsWith("]"))
								cnt_square_brackets--;
							if (elements[j].trim().endsWith("["))
								cnt_square_brackets++;
							if (elements[j].trim().startsWith("]") && 
									((cnt_square_brackets == 0) || (elements[j].trim().endsWith("[") && cnt_square_brackets==1)))
								square_element += elements[j].trim();
							if (cnt_square_brackets == 0)
								break;
						}
						// redo variable check
						final String _square_element = square_element.replaceAll("\\]\\]", "\\]").trim();
						java.util.List<ChorevolutionBPMN2ChoreographyExpressionType> result_square = explodedTypeList
								.stream()
								.filter(a -> (Objects.equals(a.name, _square_element)
										|| Objects.equals(a.name, _square_element + "[]")))
								.collect(Collectors.toList());
						if (result_square.size() > 0) {
							variable_pass = true;
						}
						evaluate_formula += element;
					}
				} else {
					String type = result.get(0).getType();
					if (type.equals("double") || type.equals("int") || type.equals("long")) {
						evaluate_formula += random.nextInt(100);
					} else if (type.equals("boolean")) {
						evaluate_formula += random.nextBoolean();
					} else {
						evaluate_formula += element;
					}
					variable_pass = true;
				}

				// *** constants ***
				Object eval = null;
				try {
					eval = evaluateJexl(element.trim());
				} catch (JexlException | StringIndexOutOfBoundsException e) {
					// do nothing
				}
				if (eval instanceof Number || eval instanceof Boolean) {
					evaluate_formula += element;
					constant_pass = true;
				}
//				 System.out.println(element + " " + operand_pass + "," +
//				 function_pass + "," + variable_pass + "," + constant_pass);

				if (!operand_pass && !function_pass && !variable_pass && !constant_pass) {
					return null;
				}
			} else {
				evaluate_formula += element;
			}
		}

		return evaluate_formula;
	}

	private void addExpressionText(String operand) {
		String _exp = textExpression.getText();
		String exp0 = _exp.substring(0, textExpression.getCaretPosition());
		String exp1 = "";
		if (_exp.length() > textExpression.getCaretPosition()) {
			exp1 = _exp.substring(textExpression.getCaretPosition(), _exp.length());
		}

		textExpression.setText(exp0 + " " + operand + " " + exp1);
		if (operand.contains("()")) {
			textExpression.setSelection(textExpression.getText().indexOf(operand) + operand.length() - 1);
		} else {
			textExpression.setSelection(textExpression.getText().indexOf(operand) + operand.length());
		}

	}

	// override method to use "OK" as label for the OK button
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, true);

		if (textExpression.getText().equals(""))// cannot submit an empty expr
			getButton(IDialogConstants.OK_ID).setEnabled(false);
	}

	@Override
	protected void okPressed() {
		text_expression = textExpression.getText();

		evaluateFormula(textExpression.getText());

		super.okPressed();
	}

	public String getExpression() {
		return text_expression;
	}

	public void setExpression(String text_expression) {
		this.text_expression = text_expression;
	}

	public boolean isValid_expression() {
		return valid_expression;
	}

	public void setValid_expression(boolean valid_expression) {
		this.valid_expression = valid_expression;
	}

	public int getType_expression() {
		return type_expression;
	}

	public void setType_expression(int type_expression) {
		this.type_expression = type_expression;
	}

	protected boolean isResizable() {
		return true;
	}
}
