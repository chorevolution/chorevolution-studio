/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension;

import org.eclipse.bpmn2.modeler.core.merrimac.clad.DefaultPropertySection;
import org.eclipse.bpmn2.modeler.core.utils.BusinessObjectUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

import eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.ChorevoutionBPMN2ChoreographyExtensionPlugin;

/**
 * The AbstractProcessPropertySection is the basic class for all  property
 * Sections. The class overwrite the doReplaceTab method to verify if a specific
 * property tab should be replaced. The property tab will only be replaced if
 * the selected EObject matches the  NameSpace. So for all other model
 * elements the property section will not replace a existing.
 *
 */
public class AbstractPropertySection extends DefaultPropertySection {

	/**
	 * This method inspects the object to determine if the namespace matches the
	 *  targetNamespace. Only in this case the given property tab will be
	 * replaced.
	 */
	@Override
	public boolean doReplaceTab(String id, IWorkbenchPart part,
			ISelection selection) {

		EObject businessObject = BusinessObjectUtil
				.getBusinessObjectForSelection(selection);

		if (ChorevoutionBPMN2ChoreographyExtensionPlugin.isCatchEvent(businessObject)) {
			return true;
		}
		return false;
	}

}
