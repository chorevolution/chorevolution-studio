/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.ui;

import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.modeler.core.merrimac.clad.AbstractBpmn2PropertySection;
import org.eclipse.bpmn2.modeler.core.merrimac.clad.AbstractDetailComposite;
import org.eclipse.bpmn2.modeler.core.merrimac.dialogs.TextObjectEditor;
import org.eclipse.swt.widgets.Composite;

import eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.ChorevoutionBPMN2ChoreographyExtensionPlugin;
import eu.chorevolution.studio.eclipse.bpmn2.choreography.extension.model.Value;

public abstract class ChorevolutionBPMN2ChoreographyDetailComposite extends AbstractDetailComposite {

	public ChorevolutionBPMN2ChoreographyDetailComposite(AbstractBpmn2PropertySection section) {
		super(section);
	}

	public ChorevolutionBPMN2ChoreographyDetailComposite(Composite parent, int style) {
		super(parent, style);

	}

	
	/**
	 * This method binds a item EObject by a given name to a TextEditor component.
	 * 
	 * @param be
	 * @param itemName
	 * @param defaultValue
	 * @param label
	 */
	public void bindTextEditor(Composite parent,BaseElement be, String itemName, String defaultValue,
			String label) {

		Value value = ChorevoutionBPMN2ChoreographyExtensionPlugin.getItemValueByName(be, itemName, null,
				defaultValue);
		TextObjectEditor valueEditor = new TextObjectEditor(this, value,
				ChorevoutionBPMN2ChoreographyExtensionPlugin._ITEMVALUE);
		valueEditor.createControl(parent, label);
	}

}
